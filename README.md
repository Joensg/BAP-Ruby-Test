# BAP-Ruby-Test
BAP Ruby Test

### Setup

* Ruby version
`ruby 2.4.0p0`

You can run the app by doing,
```
bundle install

ruby ./image_editor_app.rb
```

* How to run the test suite
```
bundle install
bundle exec rspec
```

### Rspec output summary
```
Command
  has regex constants
  .supported_commands
    returns with supported commands
  .supported?
    when input matches a supported command
      returns true
    when input does not match a supported command
      returns false
  .new
    returns an instance of Command
    with valid user input
      instance of command has data set
    with invalid user input
      instance of command has no data set
  #run
    when code I
      creates a new image
    when code C
      clears the current image
    when code L
      colours the specified pixel
    when code V
      draws a vertical segment
    when code H
      draws a horizontal segment
    when code F
      fills the region with colour
    when code S
      shows the current image
    when code X
      exits the program

ImageEditorSim
  input from command line
    gets input from command line
    translates input into command

Image
  creates an array of M x N dimensions
  created array contains M x N pixels
  when provided dimensions are out of bounds
    raises an exception
  #clear
    when existing image has non-white pixels
      sets all pixels to white
  #colour_pixel
    when existing image has all white pixels
      changes the specified pixel's colour
  #vertical_segment
    when existing image has all white pixels
      draws a vertical segment of the specified colour
  #horizontal_segment
    when existing image has all white pixels
      draws a horizontal segment of the specified colour
  #fill_region
    when existing image has all white pixels
      fills the whole image with the specified colour
    when existing image has pixels with different colours
      fills the region with the specified colour
  #show
    when existing image has all white pixels
      shows the current state of the image
    when existing image has pixels with different colours
      shows the current state of the image

Pixel
  can create an instance of Pixel
  has a position of the form (x,y), where x denotes the column and y denotes the row
  has a colour

Finished in 0.05743 seconds (files took 0.12126 seconds to load)
31 examples, 0 failures
```

### Sample Output
```
I 11 12
S
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
L 3 4 G
S
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOGOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
V 5 1 12 J
S
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOGOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
H 1 11 6 K
S
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOGOJOOOOOO
OOOOJOOOOOO
KKKKKKKKKKK
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
F 3 2 S
S
SSSSJOOOOOO
SSSSJOOOOOO
SSSSJOOOOOO
SSGSJOOOOOO
SSSSJOOOOOO
KKKKKKKKKKK
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
OOOOJOOOOOO
C
S
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
OOOOOOOOOOO
X
```

### Future enhancements
Given more time, some of the future enhancements would be
- writing some more code, refactoring, unit and integration tests around the following scenarios
	- a user not passing in the required params
  - a user entering a value for a pixel beyond the image boundries
- adding in some command line help docs to list the supported commands and how to use them.
- develop some views to be able to easily interact with the app, like on a webpage with real colours, ideally using a front-end framework.
- develop the app further to be able to import a real image and implement some basic processing on the pixels.
