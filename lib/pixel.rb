class Pixel
  attr_accessor :position, :colour

  def initialize(args)
    @position = args[:position]
    @colour = args[:colour]
  end
end
