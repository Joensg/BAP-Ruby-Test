require './lib/pixel'

class Image
  attr_reader :dimensions, :content

  def initialize(args)
    provided_dimensions = args[:dimensions].map(&:to_i)

    unless dimensions_within_bounds?(provided_dimensions)
      raise ArgumentError, 'provided column or row dimensions are out of bounds.'
    end

    @dimensions = provided_dimensions
    @content = create_pixels
  end

  def clear
    content.each do |pixel|
      pixel.colour = 'O'
    end
  end

  def colour_pixel(args)
    pixel_to_modify = content.find do |pixel|
      pixel.position == args[:position]
    end

    pixel_to_modify.colour = args[:colour]
  end

  def vertical_segment(args)
    pixels_to_modify = (args[:row_1]..args[:row_2]).map do |row|
      content.find do |pixel|
        pixel.position == [ args[:col], row ]
      end
    end

    pixels_to_modify.compact

    pixels_to_modify.each do |pixel|
      pixel.colour = args[:colour]
    end
  end

  def horizontal_segment(args)
    pixels_to_modify = (args[:col_1]..args[:col_2]).map do |col|
      content.find do |pixel|
        pixel.position == [ col, args[:row] ]
      end
    end

    pixels_to_modify.compact

    pixels_to_modify.each do |pixel|
      pixel.colour = args[:colour]
    end
  end

  def fill_region(args)

    provided_pixel = content.find do |pixel|
      pixel.position == [ args[:col], args[:row] ]
    end

    flood_fill({
      col: args[:col],
      row: args[:row],
      initial_colour: provided_pixel.colour,
      target_colour: args[:colour]
    })
  end

  def show
    column_total = dimensions[0]
    row_total = dimensions[1]

    (1..row_total).each do |row|
      (1..column_total).each do |col|
        pixel_to_display = content.find do |pixel|
          pixel.position == [col, row]
        end
        print pixel_to_display.colour
      end
      puts "\n"
    end
  end

  private

  def dimensions_within_bounds?(dimensions)
    column_total = dimensions[0]
    row_total = dimensions[1]

    valid_column = (column_total >= 1) && (column_total <= 250)
    valid_row = (row_total >= 1) && (row_total <= 250)

    valid_column && valid_row
  end

  def create_pixels
    column_total = dimensions[0]
    row_total = dimensions[1]

    pixels = []

    (1..column_total).each do |col|
      (1..row_total).each do |row|
        pixels << Pixel.new({
          position: [col, row],
          colour: 'O'
        })
      end
    end

    return pixels
  end

  def flood_fill(args)
    current_pixel = content.find do |pixel|
      pixel.position == [ args[:col], args[:row] ]
    end

    return unless current_pixel

    return if (
      (current_pixel.colour != args[:initial_colour]) ||
      (current_pixel.colour == args[:target_colour])
    )

    initial_colour = current_pixel.colour

    current_pixel.colour = args[:target_colour]

    # go north
    flood_fill({
      col: args[:col],
      row: args[:row]-1,
      initial_colour: initial_colour,
      target_colour: args[:target_colour]
    })

    # go south
    flood_fill({
      col: args[:col],
      row: args[:row]+1,
      initial_colour: initial_colour,
      target_colour: args[:target_colour]
    })

    # go east
    flood_fill({
      col: args[:col]+1,
      row: args[:row],
      initial_colour: initial_colour,
      target_colour: args[:target_colour]
    })

    # go west
    flood_fill({
      col: args[:col]-1,
      row: args[:row],
      initial_colour: initial_colour,
      target_colour: args[:target_colour]
    })

    return
  end
end
