require './lib/image'

class Command
  CREATE_IMAGE_REGEX = /^(?<code>[I]) (?<col>\d+) (?<row>\d+)$/
  CLEAR_IMAGE_REGEX = /^(?<code>[C])$/
  COLOUR_PIXEL_REGEX = /^(?<code>[L]) (?<col>\d+) (?<row>\d+) (?<colour>[A-Z])$/
  VERTICAL_SEGMENT_REGEX = /^(?<code>[V]) (?<col>\d+) (?<row_1>\d+) (?<row_2>\d+) (?<colour>[A-Z])$/
  HORIZONTAL_SEGMENT_REGEX = /^(?<code>[H]) (?<col_1>\d+) (?<col_2>\d+) (?<row>\d+) (?<colour>[A-Z])$/
  FILL_REGION_REGEX = /^(?<code>[F]) (?<col>\d+) (?<row>\d+) (?<colour>[A-Z])$/
  SHOW_IMAGE_REGEX = /^(?<code>[S])$/
  TERMINATE_SESSION_REGEX = /^(?<code>[X])$/

  attr_reader :data

  def initialize(user_input)
    match_data = parse(user_input)
    @data = match_data
  end

  def self.supported_commands
    [
      { code: 'I', regex: CREATE_IMAGE_REGEX },
      { code: 'C', regex: CLEAR_IMAGE_REGEX },
      { code: 'L', regex: COLOUR_PIXEL_REGEX },
      { code: 'V', regex: VERTICAL_SEGMENT_REGEX },
      { code: 'H', regex: HORIZONTAL_SEGMENT_REGEX },
      { code: 'F', regex: FILL_REGION_REGEX },
      { code: 'S', regex: SHOW_IMAGE_REGEX },
      { code: 'X', regex: TERMINATE_SESSION_REGEX }
    ]
  end

  def self.supported?(user_input)
    supported_commands.any? do |supported_command|
      user_input.match?(supported_command[:regex])
    end
  end

  def run(current_image)
    case data['code']
    when 'I'
      current_image = Image.new({ dimensions: [ data['col'], data['row'] ] })
    when 'C'
      current_image.clear
    when 'L'
      current_image.colour_pixel({
        position: [ data['col'].to_i, data['row'].to_i ],
        colour: data['colour']
      })
    when 'V'
      current_image.vertical_segment({
        col: data['col'].to_i,
        row_1: data['row_1'].to_i,
        row_2: data['row_2'].to_i,
        colour: data['colour']
      })
    when 'H'
      current_image.horizontal_segment({
        col_1: data['col_1'].to_i,
        col_2: data['col_2'].to_i,
        row: data['row'].to_i,
        colour: data['colour']
      })
    when 'F'
      current_image.fill_region({
        col: data['col'].to_i,
        row: data['row'].to_i,
        colour: data['colour']
      })
    when 'S'
      current_image.show
    when 'X'
      exit
    else
      "Command not supported."
    end

    current_image
  end

  private

  def parse(user_input)
    match_data = nil

    self.class.supported_commands.map do |supported_command|
      match_found = user_input.match(supported_command[:regex])

      if match_found
        match_data = match_found.named_captures
      end
    end

    return match_data
  end
end
