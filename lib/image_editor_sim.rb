require './lib/command'

class ImageEditorSim

  def initialize
    current_image_state = nil

    100.times do
      user_input = gets.chomp

      if Command.supported?(user_input)
        command = Command.new (user_input)
        current_image_state = command.run(current_image_state)
      else
        puts 'Sorry! This command is not supported. Please enter a valid command'
      end
    end
  end
end
