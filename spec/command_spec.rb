require 'command.rb'

RSpec.describe Command do
  let(:command_params) { 'I 5 6' }

  subject(:command) { described_class.new command_params }

  it 'has regex constants' do
    expect(described_class::CREATE_IMAGE_REGEX).to eq(/^(?<code>[I]) (?<col>\d+) (?<row>\d+)$/)
    expect(described_class::CLEAR_IMAGE_REGEX).to eq(/^(?<code>[C])$/)
    expect(described_class::COLOUR_PIXEL_REGEX).to eq(/^(?<code>[L]) (?<col>\d+) (?<row>\d+) (?<colour>[A-Z])$/)
    expect(described_class::VERTICAL_SEGMENT_REGEX).to eq(/^(?<code>[V]) (?<col>\d+) (?<row_1>\d+) (?<row_2>\d+) (?<colour>[A-Z])$/)
    expect(described_class::HORIZONTAL_SEGMENT_REGEX).to eq(/^(?<code>[H]) (?<col_1>\d+) (?<col_2>\d+) (?<row>\d+) (?<colour>[A-Z])$/)
    expect(described_class::FILL_REGION_REGEX).to eq(/^(?<code>[F]) (?<col>\d+) (?<row>\d+) (?<colour>[A-Z])$/)
    expect(described_class::SHOW_IMAGE_REGEX).to eq(/^(?<code>[S])$/)
    expect(described_class::TERMINATE_SESSION_REGEX).to eq(/^(?<code>[X])$/)
  end

  describe '.supported_commands' do
    let(:supported_commands) do
      [
        { code: 'I', regex: described_class::CREATE_IMAGE_REGEX },
        { code: 'C', regex: described_class::CLEAR_IMAGE_REGEX },
        { code: 'L', regex: described_class::COLOUR_PIXEL_REGEX },
        { code: 'V', regex: described_class::VERTICAL_SEGMENT_REGEX },
        { code: 'H', regex: described_class::HORIZONTAL_SEGMENT_REGEX },
        { code: 'F', regex: described_class::FILL_REGION_REGEX },
        { code: 'S', regex: described_class::SHOW_IMAGE_REGEX },
        { code: 'X', regex: described_class::TERMINATE_SESSION_REGEX }
      ]
    end

    subject(:get_supported_commands) { described_class.supported_commands }

    it 'returns with supported commands' do
      expect(get_supported_commands).to match(supported_commands)
    end
  end

  describe '.supported?' do
    let(:user_input) { '' }

    subject(:get_supported_commands) { described_class.supported?(user_input) }

    context 'when input matches a supported command' do
      let(:user_input) { 'I 5 6' }

      it 'returns true' do
        expect(get_supported_commands).to be true
      end
    end

    context 'when input does not match a supported command' do
      let(:user_input) { 'test command' }

      it 'returns false' do
        expect(get_supported_commands).to be false
      end
    end
  end

  describe '.new' do
    let(:user_input) { '' }

    subject(:command) { described_class.new(user_input) }

    it 'returns an instance of Command' do
      expect(command).to be_a(Command)
    end

    context 'with valid user input' do
      let(:user_input) { 'I 5 6' }

      it 'instance of command has data set' do
        expect(command.data).not_to be nil
      end
    end

    context 'with invalid user input' do
      let(:user_input) { 'test input' }

      it 'instance of command has no data set' do
        expect(command.data).to be nil
      end
    end
  end

  describe '#run' do
    let(:command) { described_class.new(user_input) }
    let(:current_image) { Image.new( { dimensions: [5,6] } ) }

    subject(:run_command) { command.run(current_image) }

    context 'when code I' do
      let(:user_input) { 'I 5 6' }

      before do
        allow(Image).to receive(:new)
      end

      subject(:run_command) { command.run(nil) }

      it 'creates a new image' do
        run_command

        expect(Image).to have_received(:new)
      end
    end

    context 'when code C' do
      let(:user_input) { 'C' }

      before do
        allow_any_instance_of(Image).to receive(:clear)
      end

      it 'clears the current image' do
        run_command

        expect(current_image).to have_received(:clear)
      end
    end

    context 'when code L' do
      let(:user_input) { 'L 2 3 D' }

      before do
        allow_any_instance_of(Image).to receive(:colour_pixel)
      end

      it 'colours the specified pixel' do
        run_command

        expect(current_image).to have_received(:colour_pixel)
      end
    end

    context 'when code V' do
      let(:user_input) { 'V 2 3 6 D' }

      before do
        allow_any_instance_of(Image).to receive(:vertical_segment)
      end

      it 'draws a vertical segment' do
        run_command

        expect(current_image).to have_received(:vertical_segment)
      end
    end

    context 'when code H' do
      let(:user_input) { 'H 2 3 5 D' }

      before do
        allow_any_instance_of(Image).to receive(:horizontal_segment)
      end

      it 'draws a horizontal segment' do
        run_command

        expect(current_image).to have_received(:horizontal_segment)
      end
    end

    context 'when code F' do
      let(:user_input) { 'F 3 4 D' }

      before do
        allow_any_instance_of(Image).to receive(:fill_region)
      end

      it 'fills the region with colour' do
        run_command

        expect(current_image).to have_received(:fill_region)
      end
    end

    context 'when code S' do
      let(:user_input) { 'S' }

      before do
        allow_any_instance_of(Image).to receive(:show)
      end

      it 'shows the current image' do
        run_command

        expect(current_image).to have_received(:show)
      end
    end

    context 'when code X' do
      let(:user_input) { 'X' }

      it 'exits the program' do
        lambda do
          allow(object).to receive(:exit)
          run_command
          expect(object).to have_received(:exit)
        end
      end
    end
  end
end
