require 'image_editor_sim.rb'

RSpec.describe ImageEditorSim do

  describe 'input from command line' do
    before do
      allow_any_instance_of(described_class)
      .to receive(:gets).and_return("I 5 6\n")

      allow_any_instance_of(Command)
      .to receive(:run)

      allow(Command).to receive(:new).and_return(Command.new("I 5 6"))
    end

    subject(:image_editor_sim) { described_class.new }

    it 'gets input from command line' do
      expect(image_editor_sim).to have_received(:gets).exactly(100).times
    end

    it 'translates input into command' do
      image_editor_sim
      expect(Command).to have_received(:new).with('I 5 6').exactly(100).times
    end
  end
end
