require 'pixel.rb'

RSpec.describe Pixel do
  let(:pixel_params) do
    {
      position: [2, 3],
      colour: 'O'
    }
  end

  subject(:pixel) { described_class.new pixel_params }

  it 'can create an instance of Pixel' do
    expect(pixel).to be_kind_of(Pixel)
  end

  it 'has a position of the form (x,y), where x denotes the column and y denotes the row' do
    expect(pixel.position).to eq(pixel_params[:position])
  end

  it 'has a colour' do
    expect(pixel.colour).to eq(pixel_params[:colour])
  end
end
