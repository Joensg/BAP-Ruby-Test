require 'image.rb'

RSpec.describe Image do
  let(:image_params) { { dimensions: [2, 3] } }
  let(:image) { described_class.new image_params }
  let(:expected_output) do
    "OO\n" +
    "OO\n" +
    "OO\n"
  end

  it 'creates an array of M x N dimensions' do
    expect(image.dimensions).to eq(image_params[:dimensions])
  end

  it 'created array contains M x N pixels' do
    image.content.each do |pixel|
      expect(pixel).to be_kind_of(Pixel)
    end

    expect{ image.show }.to output(expected_output).to_stdout
  end

  context 'when provided dimensions are out of bounds' do
    let(:image_params) { { dimensions: [2, 251] } }

    it 'raises an exception' do
      expect { image }.to raise_error(ArgumentError)
    end
  end

  describe '#clear' do
    context 'when existing image has non-white pixels' do
      before do
        3.times { image.content.sample.colour = 'C' }
      end

      subject(:clear_image) { image.clear }

      it 'sets all pixels to white' do
        clear_image

        expect{ image.show }.to output(expected_output).to_stdout
      end
    end
  end

  describe '#colour_pixel' do
    context 'when existing image has all white pixels' do
      before do
        image.clear
      end
      let(:expected_output) do
        "OO\n" +
        "CO\n" +
        "OO\n"
      end

      subject(:colour_pixel) { image.colour_pixel( { position: [1, 2], colour: 'C' } ) }

      it 'changes the specified pixel\'s colour' do
        colour_pixel

        expect{ image.show }.to output(expected_output).to_stdout
      end
    end
  end

  describe '#vertical_segment' do
    context 'when existing image has all white pixels' do
      before do
        image.clear
      end
      let(:expected_output) do
        "OO\n" +
        "OC\n" +
        "OC\n"
      end

      subject(:vertical_segment) do
        image.vertical_segment( { col: 2, row_1: 2, row_2: 3, colour: 'C' } )
      end

      it 'draws a vertical segment of the specified colour' do
        vertical_segment

        expect{ image.show }.to output(expected_output).to_stdout
      end
    end
  end

  describe '#horizontal_segment' do
    context 'when existing image has all white pixels' do
      before do
        image.clear
      end
      let(:expected_output) do
        "OO\n" +
        "CC\n" +
        "OO\n"
      end

      subject(:horizontal_segment) do
        image.horizontal_segment( { col_1: 1, col_2: 2, row: 2, colour: 'C' } )
      end

      it 'draws a horizontal segment of the specified colour' do
        horizontal_segment

        expect{ image.show }.to output(expected_output).to_stdout
      end
    end
  end

  describe '#fill_region' do
    subject(:fill_region) do
      image.fill_region( { col: 1, row: 2, colour: 'C' } )
    end

    context 'when existing image has all white pixels' do
      before do
        image.clear
      end
      let(:expected_output) do
        "CC\n" +
        "CC\n" +
        "CC\n"
      end

      it 'fills the whole image with the specified colour' do
        fill_region

        expect{ image.show }.to output(expected_output).to_stdout
      end
    end

    context 'when existing image has pixels with different colours' do
      before do
        image.clear
        image.colour_pixel( { position: [2, 2], colour: 'I'} )
        image.colour_pixel( { position: [1, 3], colour: 'D'} )
      end
      let(:expected_output) do
        "CC\n" +
        "CI\n" +
        "DO\n"
      end

      it 'fills the region with the specified colour' do
        fill_region

        expect{ image.show }.to output(expected_output).to_stdout
      end
    end
  end

  describe '#show' do
    subject(:show_image) { image.show }

    context 'when existing image has all white pixels' do
      before do
        image.clear
      end

      it 'shows the current state of the image' do
        expect{ show_image }.to output(expected_output).to_stdout
      end
    end

    context 'when existing image has pixels with different colours' do
      before do
        image.clear
        image.colour_pixel( { position: [2, 2], colour: 'I'} )
        image.colour_pixel( { position: [1, 3], colour: 'D'} )
      end
      let(:expected_output) do
        "OO\n" +
        "OI\n" +
        "DO\n"
      end

      it 'shows the current state of the image' do
        expect{ show_image }.to output(expected_output).to_stdout
      end
    end
  end
end
